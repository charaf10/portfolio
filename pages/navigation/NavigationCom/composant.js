import Head from 'next/head'
import Image from 'next/image'
import styles from '../../../styles/Home.module.css'
import { ethers, Signer } from 'ethers'
import Web3Modal from 'web3modal'
//import Script from '../../../javascript/apiCMC.js'



export default function composant() {


let composant = 



<div className={styles.container}>


         <div className={styles.bismi}>
            <p>  بسم الله الرحمن الرحيم &nbsp;</p>
        </div>

        <div className={styles.divNavigation}>
            <ul className={styles.nav}>
            <li className={styles.logoo} ><a className={styles.alogo}  href="/dossierTest/test">Zone Test</a></li>

            <li className={styles.li}><a className={styles.a} href="/">Home</a></li>
            <li className={styles.li}><a className={styles.a}  href="/Post/Post">Post</a></li>
            <li className={styles.li}><a className={styles.a}  href="/exchange/ExchangePage">Exchange</a></li>
            <li className={styles.li}><a className={styles.a}  href="/Roadmap/Roadmap">Roadmap</a></li>

            <li className={styles.li}><a className={styles.a}>Lobby</a></li>

            <li className={styles.li}><a className={styles.a}  href="/OpenSource/Opensource">Projet O/S</a></li>
            <li className={styles.li}><a className={styles.a}  href="/Client/Client">Client</a></li>
            <li className={styles.li}><a className={styles.a}  href="/Report/Report">Report</a></li>
            <li className={styles.li}><a className={styles.a}  href="/About/About">About</a></li>
            <li className={styles.login} ><a id='connect' onClick={ConnectWallet} className={styles.alogin}>Connect<br/>wallet</a></li>
            

            </ul>
        </div>
</div>

return(
    
    composant)
}


async function ConnectWallet()  
{
    try {
        const web3Modal = new Web3Modal()
        const connection = await web3Modal.connect()
        const provider = new ethers.providers.Web3Provider(connection)
        const signer = provider.getSigner()

        const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
        document.getElementById('connect').textContent = accounts[0]
        
    } catch (error) {
        console.log(error)
    }


}
