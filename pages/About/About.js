import navigation2 from '../navigation/navigation.js'
import scriptt from '../../javascript/scriptAbout.js'
import about from '../About/AboutComp/AboutComp.js'



export default function About() {

// recuperation des datas recu par la page en question sous forme d'objet 
//(child of object contains data)
  let navigation = navigation2().props.children
  let aboutpage = about().props.children
  
    let page =
      <div>
          {navigation}
          {aboutpage}
      </div>
  return (page)
}
