import composant from './ExchangeCom/composant.js'
import navigation2 from '../navigation/navigation.js'

export default function exchangePage() {


    let navi = navigation2().props.children
    let compo = composant().props.children

    let page = 
            <div>
                {navi}
                {compo}
            </div>
    return (page)
} 