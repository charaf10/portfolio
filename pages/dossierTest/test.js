import Head from 'next/head'
import Image from 'next/image'
import styles from '../../styles/Home.module.css'
import navigation2 from '../navigation/navigation.js'
import composant1 from '../dossierTest/TestComp/composant1.js'


export default function test() {


  let navigation = navigation2().props.children
  let composant = composant1().props.children
  
  let page =
    <div>
        {navigation}
        {composant}
    </div>
  return (page)
}
