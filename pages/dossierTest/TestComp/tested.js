import styles from '../../../styles/Home.module.css'
/*web3 pack */
import { ethers } from 'ethers'
import Web3Modal from 'web3modal'
import {TestAddress} from '../../../config'
import ContractDetest from '../../../artifacts/contracts/ContractDetest.sol/ContractDetest.json'


export default function composant1() {

  let test = 
  <div className={styles.divTotal}>

      <br/>
      <div>
        <h1 className={styles.titre}>tested component</h1>



        <div className={styles.signupForm}>
          <h1>Test: Deploy Contract Onclick</h1>
          <br/>
          <button onClick={deploy} className={styles.inputFields} id="join-btn" name="join" alt="Join">
          Click to deploy contract ...  
          </button>
          <br/>
          <span id='infodeploy'> Resultat : </span>
        </div>



        <div className={styles.signupForm}>
          <h1>Test: call get_function of deployed contract</h1>
          <br/>
          <button onClick={CallHello} className={styles.inputFields} id="join-btn" name="join" alt="Join">
          Call fonction (hello world!) from contract...
          </button>
          <br/>
          <span id='reponseget'> Resultat : </span>
        </div>



        <div>
            <div className={styles.signupForm}>
            <h1>Test: Write on deployed contract</h1>
            <br/>
            <input type="text" className={styles.inputFields2} id="newMsg"  placeholder="Enter the new text" required/>
            <button onClick={updateText} className={styles.inputFields} id="join-btn" name="join" alt="Join">
            Update Le message
            </button>
            <br/>
            <span id='reponsepost'>Resultat : </span>
            </div>
        </div>


      </div>
  </div> 

  return (test)
}


/*=============================================================================== */

/*LES FONCTIONS PLUGIN BUTTON */
async function deploy(){
  try {
    const req = await fetch("/api/execute");
    const data = await req.json();
    console.log(data);
    document.getElementById('infodeploy').textContent = data; 
  } catch (error) {
    document.getElementById('infodeploy').textContent = error; 
  }
}


/*fonction qui appelle la fonction 'helloworld' qui se trouve dans le smart contract */
async function CallHello()  {
  try {
      const web3Modal = new Web3Modal()
      const connection = await web3Modal.connect()
      const provider = new ethers.providers.Web3Provider(connection)
      const signer = provider.getSigner()

      /* get contract by abi */
      let contract = new ethers.Contract(TestAddress, ContractDetest.abi, signer)
      let transaction1 = await contract.helloWorld()

      console.log('tranzaction1: ', transaction1 )
      document.getElementById('reponseget').textContent = transaction1; 
    } catch (error) {
      document.getElementById('reponseget').textContent = error ; 
    }
}



    /*fonction qui appelle la fonction 'settext' qui se trouve dans le smart contract */
    async function updateText()  {
      try {
      const web3Modal = new Web3Modal()
      const connection = await web3Modal.connect()
      const provider = new ethers.providers.Web3Provider(connection)
      const signer = provider.getSigner()
    
      /* get contract by abi */
      let contract = new ethers.Contract(TestAddress, ContractDetest.abi, signer)
      let mesage = document.getElementById('newMsg').value;
      let transaction1 = await contract.setText(mesage)
    
      console.log('tranzaction1: ', transaction1 )
      document.getElementById('reponsepost').textContent = transaction1; 
    } catch (error) {
      document.getElementById('reponsepost').textContent = error; 
    }
}



