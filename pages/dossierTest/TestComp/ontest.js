import styles from '../../../styles/Home.module.css'
/*web3.storage pack*/
import Web3Modal from 'web3modal'
import { Web3Storage } from 'web3.storage';
/*web3 pack */
import { Contract, ethers } from 'ethers'
import { getFilesFromPath } from 'web3.storage';

/*
import ContractDetest from '../../../artifacts/contracts/ContractDetest.sol/ContractDetest.json'
import {TestAddress} from '../../../config'
*/

import ContractReceiveEther from '../../../artifacts/contracts/Recevoir.sol/Recevoir.json'
import {TestAddressrece} from '../../../configrece'
  
import ContractSendEther from '../../../artifacts/contracts/Envoyer.sol/Envoyer.json'
import {TestAddressenv} from '../../../configenv'

export default function composant1() {

  let test = 
  <div className={styles.divTotal}>

    <br/>
    <div>
        <h1 className={styles.titre}>Ontest component</h1>


        <div className={styles.signupForm}>
          <h1>Test: Transfer token from account 1 to account 2.</h1>
          <br/>
          <button onClick={deploy2} className={styles.inputFields} id="join-btn" name="join" alt="Join">
          Deploy the contract
          </button>
          <span id='infoDeploy2'>Resultat : </span>
          <br/>
          <input type="number" className={styles.inputFields2} id="Amount" name="Amount" placeholder="Amount" required/>
          <br/> 
          <button onClick={transfert} className={styles.inputFields} id="join-btn" name="join" alt="Join">
          transferer le montant
          </button>
          <br/>
          <span id='infoTransfert'>Resultat : </span>
          <br/> 
          <button onClick={recevoir} className={styles.inputFields} id="join-btn" name="join" alt="Join">
          getBalance receive...
          </button>
          <br/>
            <span id='infoReceive'>Resultat : </span>
        </div>



        <div className={styles.signupForm}>
          <h1>Test: upload file to ipfs onclick</h1>
          <br/>
          <button className={styles.inputFields} id="join-btn" name="join" alt="Join">
          Upload files...
          </button>
          <br/>
            <span>Resultat : </span>
        </div>



        <div className={styles.signupForm}>
          <h1>Test: Start node on terminal</h1>
          <button onClick={startTerminal} className={styles.inputFields} id="join-btn" name="join" alt="Join">
          Start terminal...
          </button>
          <br/>
          <span id='infoTerminal'> Resultat : </span>
          <br/>
          <br/>
          <button onClick={startNode} className={styles.inputFields} id="join-btn" name="join" alt="Join">
          Start node...  
          </button>
          <br/>
          <span id='infoNode'> Resultat : </span>
        </div>



          <div className={styles.signupForm}>
            <h1>Test: Get one file from ipfs onclick</h1>
            <br/>
            <button onClick={getfileIpfs} className={styles.inputFields} id="join-btn" name="join" alt="Join">
            Get files...
            </button>
            <br/>
            <span id='infoGetIpfs'>Resultat : </span>
          </div>
    </div>
  </div> 
  return (test)
}



/*LES FONCTIONS BRANCHER AU <BUTTON/> */

async function deploy2(){
  try {
    const req = await fetch("/api/executeEnvoyer");
    const data = await req.json();
    console.log(data);
    document.getElementById('infoDeploy2').textContent = data; 
  } catch (error) {
    document.getElementById('infoDeploy2').textContent = error; 
  }
}
async function transfert(){
    try {
      const web3Modal = new Web3Modal()
      const connection = await web3Modal.connect()
      const provider = new ethers.providers.Web3Provider(connection)
      const signer = provider.getSigner()

      /* get contract by abi */
      
      let prix = document.getElementById('Amount').value
      let contract = new ethers.Contract(TestAddressenv, ContractSendEther.abi, signer)
      let transaction1 = await contract.sendViaCall('0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0', {value: prix})
      console.log('tranzaction1: ', transaction1 ) 
      document.getElementById('infoTransfert').textContent = "transaction complete!"; 
  } catch (error) {
      document.getElementById('infoTransfert').textContent = error; 

  }
}



async function recevoir(){
  try {

    const web3Modal = new Web3Modal()
    const connection = await web3Modal.connect()
    const provider = new ethers.providers.Web3Provider(connection)
    const signer = provider.getSigner()


    /* get contract by abi */
    let contract = new ethers.Contract(TestAddressrece, ContractReceiveEther.abi, signer)
    let transaction1 = await contract.getBalance();
    console.log('tranzactionReceive: ', transaction1 ) 
    document.getElementById('infoReceive').textContent = transaction1; 
} catch (error) {
  document.getElementById('infoReceive').textContent = error; 

}
}


async function startTerminal(){

  try {
    const req = await fetch("/api/startTerminal");
    const data = await req.json();
    console.log(data);
    document.getElementById('infoTerminal').textContent = "the Terminal is started"; 
  } catch (error) {
    document.getElementById('infoTerminal').textContent = error; 

  }
}


async function startNode(){

  try {
    const req = await fetch("/api/startNode");
    const data = await req.json();
    console.log(data);
    document.getElementById('infoNode').textContent = "the node is started"; 
  } catch (error) {
    document.getElementById('infoNode').textContent = error; 
  }
}


async function getfileIpfs(){

  try {
    //const files = await getFilesFromPath('/image');
    //const rootCid = await client.put(files);
    console.log("le root cid ");
  } catch (error) {
    //document.getElementById('infoGetIpfs').textContent = error; 
  }
}






