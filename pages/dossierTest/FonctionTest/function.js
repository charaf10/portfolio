


async function fonctionTest(){

    try {
      const req = await fetch("/api/execute");
      const data = await req.json();
      console.log(data);
      document.getElementById('info').textContent = data; 

    } catch (error) {
      document.getElementById('info').textContent = error; 

    }
  }


/*fonction qui appelle la fonction 'helloworld' qui se trouve dans le smart contract */
  async function CallHello()  {
    const web3Modal = new Web3Modal()
    const connection = await web3Modal.connect()
    const provider = new ethers.providers.Web3Provider(connection)
    const signer = provider.getSigner()
  
    /* get contract by abi */
    let contract = new ethers.Contract(LockAddress, Lock.abi, signer)
    let transaction1 = await contract.helloWorld()
  
    //await transaction1.wait
    //router.push('/')
  
    console.log('tranzaction1: ', transaction1 )
    document.getElementById('reponse').textContent += transaction1; 

  }