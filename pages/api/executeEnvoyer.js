
//import { execSync } from 'child_process';  // replace ^ if using ES modules
export default function handler(req, res) {
    const execSync = require('child_process').execSync;
  //C:\Users\Charaf\Desktop\projet-web3\test-payable\test-payable\test-payable\scripts\deploy.js
  
      const output = execSync('npx hardhat run scripts/deployEnvoyer.js --network localhost', { encoding: 'utf-8' });  // the default is 'buffer'
      const splitted = output.split(/\r?\n/);  
      const filtered = splitted.filter( e => {
        return e !== '';
      });
    
      res.status(200).json(JSON.stringify(filtered))
    }