import navigation2 from '../../pages/navigation/navigation.js'
import composant from './PostCom/composant.js'


export default function Post() {

// recuperation des datas recu par la page en question sous forme d'objet 
//(child of object contains data)
  let navigation = navigation2().props.children
  let compo = composant().props.children

  let page = 

  <div>
        {navigation}   
        {compo}
  </div>


  return (page)
}
