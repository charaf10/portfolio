const hre = require("hardhat");
const fs = require('fs');

async function main() {
  const Testt = await hre.ethers.getContractFactory("Recevoir");
  const ContractTestt = await Testt.deploy();
  await ContractTestt.deployed();
  console.log("Contract deployed to:", ContractTestt.address);
  fs.writeFileSync('./configrece.js', `
  export const TestAddressrece = "${ContractTestt.address}"
  `)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });