const hre = require("hardhat");
const fs = require('fs');

async function main() {
  const Test = await hre.ethers.getContractFactory("ContractDetest");
  const ContractTest = await Test.deploy();
  await ContractTest.deployed();
  console.log("Contract deployed to:", ContractTest.address);
  fs.writeFileSync('./config.js', `
  export const TestAddress = "${ContractTest.address}"
  `)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });