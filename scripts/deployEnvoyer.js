const hre = require("hardhat");
const fs = require('fs');

async function main() {
  const Testtt = await hre.ethers.getContractFactory("Envoyer");
  const ContractTesttt = await Testtt.deploy();
  await ContractTesttt.deployed();
  console.log("Contract deployed to:", ContractTesttt.address);
  fs.writeFileSync('./configenv.js', `
  export const TestAddressenv = "${ContractTesttt.address}"
  `)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });