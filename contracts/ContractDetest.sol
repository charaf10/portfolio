// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

contract ContractDetest {

    string Lemessage = "Hello world";


    function helloWorld() external view returns(string memory){
        return  Lemessage;
    }

    function setText(string memory _lemessage) public{
        
        Lemessage = _lemessage; 

    }
 }